class Elevator(object):
    """Elevator engine"""
    def __init__(self):
        super(Elevator, self).__init__()

    def call(self, atFloor, to):
        print("Called from %s to %s" % (atFloor, to))

    def floor_to_go(self, floorToGo):
        pass

    def user_has_entered(self):
        pass

    def user_has_exited(self):
        pass

    def next_command(self):
        return "NOTHING"
