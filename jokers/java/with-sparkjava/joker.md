Add this dependency to your pom.xml :

	<dependency>
		<groupId>com.sparkjava</groupId>
		<artifactId>spark-core</artifactId>
		<version>2.3</version>
	</dependency>
	
(or download [this jar](http://search.maven.org/remotecontent?filepath=com/sparkjava/spark-core/2.3/spark-core-2.3.jar) and include it in your classpath)
		
Start coding :  

```java
import static spark.Spark.*;

public class HelloWorld {
    public static void main(String[] args) {
        get("/hello", (req, res) -> "Hello World");
    }
}
```

Run and view :  

	http://localhost:4567/hello
	
[http://sparkjava.com/download.html](http://sparkjava.com/download.html)