package elevator;

import static spark.Spark.*;

public class Application {
    public static void main(String[] args) {
    	Elevator elevator = new Elevator();
    	get("/", (request, response) -> "Welcome");
    	
    	get("/reset", (request, response) -> {
    		return elevator.reset();
    	});
    	
    	get("/nextCommand", (request, response) -> "NOTHING");
    	get("/call", (request, response) -> "NOTHING");
    	get("/go", (request, response) -> "NOTHING");
    	get("/userHasEntered", (request, response) -> "NOTHING");
    	get("/userHasExited", (request, response) -> "NOTHING");
    }

}
